﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Net;

namespace TD1_WPF
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn_Quitter_Click(object sender, RoutedEventArgs e)
        {
            // Exécuter lors d'un clic sur le bouton "Quitter"
            MessageBox.Show("Naaannn Revenez.... Je peut tout expliquer !");
            Application.Current.Shutdown();
        }

        private void btn_OK_Click(object sender, RoutedEventArgs e)
        {
            IPHostEntry ihe;

            if(txtb_URL.Text == "")
            {
                MessageBox.Show("Veuillez saisir un nom de machine");
                return;
            }

            try
            {
                // Ici instruction pouvant échouer
                ihe = Dns.GetHostEntry(txtb_URL.Text);
            }
            catch(Exception ee)
            {
                // Exécuté si erreur dans le try
                // ee est alimenté par un message d'erreur
                MessageBox.Show(ee.Message);
                return;
            }
        }
    }
}
